Source: wabt
Section: devel
Priority: optional
Maintainer: Markus Koschany <apo@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 python3,
 re2c
Standards-Version: 4.6.1
Homepage: https://github.com/WebAssembly/wabt
Vcs-Git: https://salsa.debian.org/debian/wabt.git
Vcs-Browser: https://salsa.debian.org/debian/wabt

Package: wabt
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: WebAssembly Binary Toolkit
 WABT (pronounced: "wabbit") is a suite of tools for WebAssembly, including:
 .
  * wat2wasm: translate from WebAssembly text format to the WebAssembly binary
    format
  * wasm2wat: the inverse of wat2wasm, translate from the binary format back
    to the text format (also known as a .wat)
  * wasm-objdump: print information about a wasm binary. Similar to objdump.
  * wasm-interp: decode and run a WebAssembly binary file using a stack-based
    interpreter
  * wat-desugar: parse .wat text form as supported by the spec interpreter
    (s-expressions, flat syntax, or mixed) and print "canonical" flat format
  * wasm2c: convert a WebAssembly binary file to a C source and header
 .
 These tools are intended for use in (or for development of) toolchains or
 other systems that want to manipulate WebAssembly files. Unlike the
 WebAssembly spec interpreter (which is written to be as simple, declarative
 and "speccy" as possible), they are written in C/C++ and designed for easier
 integration into other systems. Unlike Binaryen these tools do not aim to
 provide an optimization platform or a higher-level compiler target; instead
 they aim for full fidelity and compliance with the spec (e.g. 1:1 round-trips
 with no changes to instructions).
